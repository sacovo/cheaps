/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fribot.cheaps;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sandro
 */
public class CheaPSUpdater implements Runnable {

    private Robot robot;

    public CheaPSUpdater(Robot robot) {
        this.robot = robot;
    }

    @Override
    public void run() {
        // Connect to the CheaPS Server
        CheaPSclient.connect(getRobot().getCheapsAddress());
        robot.setRunning(true);
        while (true) {
            try {
                String coordinate_line = CheaPSclient.readLine();
                if(coordinate_line.startsWith("GOAL:")){
                    final int goalX = new Integer(coordinate_line.split(" ")[1]);
                    final int goalY = new Integer(coordinate_line.split(" ")[2]);
                try {
                    robot.cancelMoving();
                    new Thread(){
                        @Override
                        public void run(){
                        robot.moveTo(goalX, goalY, 15);
                    }}.start();
                    continue;
                } catch (InterruptedException ex) {
                    Logger.getLogger(CheaPSUpdater.class.getName()).log(Level.SEVERE, null, ex);
                }
                }
                robot.setX((int) new Integer(coordinate_line.split(" ")[0]));
                robot.setY((int) new Integer(coordinate_line.split(" ")[1]));
                
                getRobot().getOldX().add(getRobot().getX());
                getRobot().getOldY().add(getRobot().getY());
                
            } catch (NumberFormatException e) {
                
            }
            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                Logger.getLogger(Robot.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(-1);
            }
        }
    }

    /**
     * @return the robot
     */
    public Robot getRobot() {
        return robot;
    }

    /**
     * @param robot the robot to set
     */
    public void setRobot(Robot robot) {
        this.robot = robot;
    }

}
