/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.cheaps;

import java.util.logging.Level;
import java.util.logging.Logger;

import lejos.hardware.Button;

/**
 *
 * @author sandro
 */
public class CheaPSNavigator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final Robot robot = new Robot();
        
        // Start the thread to get the coordinates
        final Thread cheapsT = new Thread(robot.getCheaPSUpdater());
        cheapsT.setDaemon(true);
        cheapsT.start();
        
        final Thread displayT = new Thread(robot.getDisplayUpdater());
        displayT.setDaemon(true);
        displayT.start();
        new Thread(new Runnable(){
            @Override
            public void run(){
                Button.ENTER.waitForPressAndRelease();
                cheapsT.interrupt();
                displayT.interrupt();
                System.exit(0);
            }
        }).start();
    }
    
}
