package org.fribot.cheaps;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

class CheaPSclient {

    private final static int PORT_NUMBER = 1337;
    private static Socket socket = null;
    private static BufferedReader inStream = null;
    private static BufferedWriter outStream = null;
    private static String lastRead = "";
    private static long lastReceived = 0L;
 
    public static boolean connect(String address) {
        Socket clientSocket=null;
        try {
            socket = new Socket(address, 1337);
        } catch (UnknownHostException ex) {
            Logger.getLogger(CheaPSclient.class.getName()).log(Level.SEVERE, "ERROR: Cannot find server "+address, ex);
            return false;
        } catch (IOException ex) {
            Logger.getLogger(CheaPSclient.class.getName()).log(Level.SEVERE, "Connection to "+address+" failed", ex);
            return false;
        }
        try {
            inStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            outStream = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException ex) {
            Logger.getLogger(CheaPSclient.class.getName()).log(Level.SEVERE, "Connection to "+address+" failed", ex);
        }
        lastReceived = System.currentTimeMillis();
        return true;
    }
    
    public static String writeLine(String line) {
        if (outStream==null){
            return "ERROR: no connection";
        }
        try {
            outStream.write(line);
            outStream.newLine();
            outStream.flush();
        } catch (IOException ex) {
           return "ERROR: connection broken";
        }
        return "";
    }

    public static String readLine() {
        if (inStream==null) {
            return "ERROR: no connection";
        }
        try {
            if (inStream.ready()) {
                while (inStream.ready()) {
                    lastReceived = System.currentTimeMillis();
                    lastRead = inStream.readLine();
                }
            } else { // check for timeout
                if (System.currentTimeMillis() - lastReceived > 3000) {
                    inStream.close();
                    outStream.close();
                    socket.close();
                    inStream = null;
                    socket = null;
                    return "ERROR: connection lost";
                }
            }
        } catch (IOException ex) {
            lastRead = "ERROR: lost connection";
            Logger.getLogger(CheaPSclient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lastRead;
    }
    
    public static void main(String[] args) throws InterruptedException {
        if (connect("localhost")) {
            while (true) {
                String line = readLine();
                System.out.println(readLine());
                writeLine("Hello World!");
                if (line.startsWith("ERROR")) {
                    break;
                }
                Thread.sleep(500);
            }
        }
    }            
}
