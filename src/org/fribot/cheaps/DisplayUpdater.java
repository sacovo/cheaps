/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.fribot.cheaps;

import java.util.logging.Level;
import java.util.logging.Logger;
import lejos.hardware.lcd.TextLCD;

/**
 *
 * @author sandro
 */
public class DisplayUpdater implements Runnable{

    private Robot robot;
    
    public DisplayUpdater(Robot robot){
        this.robot = robot;
    }
    
    @Override
    public void run() {
        while(true){
            String state = getRobot().getCurrentState();
            int x = getRobot().getX();
            int y = getRobot().getY();
            
            TextLCD lcd = getRobot().getTextLCD();
            lcd.clear();
            lcd.drawString("Position: " + x + "/" + y, 0, 0);
            lcd.drawString(state, 0, 1);
            
            String rt = CheaPSclient.writeLine(state);
            if(rt.startsWith("ERROR")){
                try{
                    robot.cancelMoving();
                    Thread.sleep(1000);
                    System.out.println("Trying to reconnect...");
                    CheaPSclient.connect(robot.getCheapsAddress());
                } catch(InterruptedException ex){
                    
                }
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(DisplayUpdater.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(-1);
            }
        }
    }

    /**
     * @return the robot
     */
    public Robot getRobot() {
        return robot;
    }

    /**
     * @param robot the robot to set
     */
    public void setRobot(Robot robot) {
        this.robot = robot;
    }
    
}
